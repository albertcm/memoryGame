package com.exemple.profedam.memory.controllers;

/**
 * Created by acm on 28/02/18.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;
import android.widget.TextView;
import com.exemple.profedam.memory.R;
import com.exemple.profedam.memory.model.Partida;

public class Game extends AppCompatActivity {

    private GridView gv;
    private Partida partida;
    private ImageAdapter adapter;
    private CountDownTimer cdown;
    private final int seconds = 1000;
    private int timeSec = 60;
    private final GeneralListener listener = new GeneralListener(this);
    public static int CONTADOR;


    public Partida getPartida() {
        return partida;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        int cartas = getIntent().getIntExtra("dificultad", -33);
        gv = findViewById(R.id.gridViewMemory);


        this.partida = new Partida(cartas);
        adapter = new ImageAdapter(this, partida);

        gv.setAdapter(adapter);
        gv.setOnItemClickListener(listener);
        contador();
    }

    public void refrescarTablero() {
        gv.setAdapter(adapter);
        gv.refreshDrawableState();
    }

    public boolean finPartidaValidacion() {
        boolean validar;
        if(CONTADOR == partida.getNumeroCartes()/2){
            validar = true;
            cdown.cancel();
        } else {
            validar =  false;
        }
        return validar;
    }

    public void finPartida(boolean timesOut) {
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle((timesOut)?getString(R.string.timeOut):getString(R.string.win));
        ad.setMessage(R.string.tryAgain);
        ad.setCancelable(false);
        ad.setPositiveButton(R.string.menu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptar();
            }
        });
        ad.setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        ad.show();
    }

    public void aceptar() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void cancelar() {
        finish();
    }

    public void contador() {
        cdown = new CountDownTimer(timeSec * 800, seconds) {

            public void onTick(long millisUntilFinished) {

                ((TextView) findViewById(R.id.contador)).setText("Tiempo Restante: " + (millisUntilFinished / seconds));
            }

            public void onFinish() {

                ((TextView) findViewById(R.id.contador)).setText("SE ACABO!");
                finPartida(true);
            }
        }.start();
    }
}

