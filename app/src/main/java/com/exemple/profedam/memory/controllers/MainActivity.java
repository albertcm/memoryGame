package com.exemple.profedam.memory.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.exemple.profedam.memory.R;



public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnCallActivity = (Button) findViewById(R.id.btnStart);
        btnCallActivity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnStart) {
            Intent i = new Intent(this, Game.class);
            RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);
            boolean exit = false;

            switch (rg.getCheckedRadioButtonId()) {
                case R.id.easy: {
                    i.putExtra("dificultad", 8);
                    exit = true;
                    break;
                }
                case R.id.medium: {
                    i.putExtra("dificultad", 10);
                    exit = true;
                    break;
                }
                case R.id.hard: {
                    i.putExtra("dificultad", 12);
                    exit = true;
                    break;
                }
                case R.id.hardcore: {
                    i.putExtra("dificultad", 16);
                    exit = true;
                    break;
                }
                default: {
                    Toast.makeText(this, "Selecciona dificultad", Toast.LENGTH_SHORT).show();

                    break;
                }
            }
            startActivity(i);
            if (exit) {
                finish();
            }
        }
    }
}

