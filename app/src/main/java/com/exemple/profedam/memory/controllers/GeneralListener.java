package com.exemple.profedam.memory.controllers;

import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import com.exemple.profedam.memory.model.Carta;
import com.exemple.profedam.memory.model.Partida;
import java.util.ArrayList;

/**
 * Created by acm on 28/02/2018.
 */
public class GeneralListener implements AdapterView.OnItemClickListener, Runnable{

    private Game tauler;
    private Carta carta;
    private boolean listenerActive = true;
    private ArrayList<Carta> listaCartasFront;


    public GeneralListener(Game tauler) {
        this.tauler = tauler;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        Partida partida = tauler.getPartida();
        carta = tauler.getPartida().getLlistaCartes().get(position);

        if (listenerActive && carta.getEstat() == Carta.Estat.BACK) {
            carta.girar();
            tauler.refrescarTablero();
            listaCartasFront = partida.mostrarFront();
            if (listaCartasFront.size() == 2 && listaCartasFront.get(0).getFrontImage() != listaCartasFront.get(1).getFrontImage()) {
                this.listenerActive = false;
                Handler delay = new Handler();
                delay.postDelayed(this, 1000);
            } else if (listaCartasFront.size() == 2) {
                listaCartasFront.get(0).setEstat(Carta.Estat.FIXED);
                listaCartasFront.get(1).setEstat(Carta.Estat.FIXED);
                Game.CONTADOR++;
            }
        }
        if(tauler.finPartidaValidacion()){
            tauler.finPartida(false);
            Game.CONTADOR = 0;
        }
    }

    @Override
    public void run() {
        listaCartasFront.get(0).girar();
        listaCartasFront.get(1).girar();
        tauler.refrescarTablero();
        listenerActive = true;

}
}

